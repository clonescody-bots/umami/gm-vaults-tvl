import Axios from 'axios';
import { Client, Intents } from 'discord.js';
import { config } from './config.js';

const client = new Client({ intents: [Intents.FLAGS.GUILDS] });
// if you don't want to update a server ran by a bozo
const kekList = ['885769815552323626'];

let gmVaultsTvl = 0.0;

const getApiLastTvl = async () => {
  try {
    const { data } = await Axios.get(
      'https://api.umami.finance/api/v2/vaults/historical-tvl',
    );
    gmVaultsTvl = parseFloat(data.historicalTvl[data.historicalTvl.length - 1].gmiVaults);

    return gmVaultsTvl;
  } catch (error) {
    console.error(error);
    return gmVaultsTvl;
  }
};

export const updateValues = async () => {
  console.log('call');
  const vaultsTvl = await getApiLastTvl();

  client.guilds.cache.map((guild) => {
    // update every server the bot is connected to
    if (guild.me) {
      const nickname = kekList.includes(guild.id.toString())
        ? 'ON STRIKE'
        : `$${parseInt(vaultsTvl.toFixed(0)).toLocaleString('en-US')}`;
      guild.me.guild.me.setNickname(nickname);
      client.user.setActivity('GM Vaults TVL', {
        type: 'WATCHING',
      });
    }
  });
};

const run = async () => {
  // update once to init then run every hour
  await updateValues();
  setInterval(updateValues, 1 * 60000);
};

client.once('ready', () => {
  console.log('Ready!');
  client.user.setActivity('GM Vaults TVL', {
    type: 'WATCHING',
  });
  run();
});

client.login(config.DISCORD_CLIENT_TOKEN);
